package sdkjordan.com.alkaff.myfirstapp2019;

public final class Constants {
    public static final String DATA_FILE_NAME = "file.txt";
    public static final String TAG = "myfirstapp2019";
    public static final String RECEIVER_TAG = "receiver";
    public static final int PERMISSION_REQUEST_CODE_CALL = 15 ;


    public static final class Actions {
        public static final String FirstAction = "sdkjordan.com.alkaff.myfirstapp2019.FirstAction";
    }

    public static final class Keys {

        public static final String HIGHEST_SCORE = "highest" ;

        public static final String CONTACTS = "contacts";
        public static final String ID = "_id";
        public static final String NAME = "name";
        public static final String PHONE = "phone";
    }

    public static final class Extras {
        public static final String EXTRA_DATA = "data";
    }

    public static final class DataBase {
        public static final String DB_NAME = "my_db";
        public static final String TABLE_NAME = "DATA";
        public static final String COLUMN_ID = Keys.ID ;
        public static final String COLUMN_NAME = Keys.NAME ;
        public static final String COLUMN_PHONE = Keys.PHONE ;


    }
}
