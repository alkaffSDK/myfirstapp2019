package sdkjordan.com.alkaff.myfirstapp2019.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;

import sdkjordan.com.alkaff.myfirstapp2019.Constants;
import sdkjordan.com.alkaff.myfirstapp2019.R;
import sdkjordan.com.alkaff.myfirstapp2019.receivers.StaticReceiver;

public class BroadCastActivity extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    EditText mEditTextData;
    Switch mASwitch;

    StaticReceiver mStaticReceiver;
    static final String RECIEVER_ACTION = "sdkjordan.com.alkaff.myfirstapp2019.ACTION_RECEIVE";
    IntentFilter mIntentFilter = new IntentFilter(RECIEVER_ACTION);

    LocalBroadcastManager localBroadcastManager;
    BroadcastReceiver anotherBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_broad_cast);


        mEditTextData = findViewById(R.id.editTextData1);
        mASwitch = findViewById(R.id.switch2);
        mASwitch.setOnCheckedChangeListener(this);

        localBroadcastManager = LocalBroadcastManager.getInstance(BroadCastActivity.this);


        /*
         *
         *  localBroadcastManager helper to register for and send broadcasts of Intents to local objects within your process.
         *  This has a number of advantages over sending global broadcasts with sendBroadcast(Intent):
         *   You know that the data you are broadcasting won't leave your app, so don't need to worry about leaking private data.
         *   It is not possible for other applications to send these broadcasts to your app, so you don't need to worry about having security holes they can exploit.
         *   It is more efficient than sending a global broadcast through the system.
         */
        mStaticReceiver = new StaticReceiver();

        anotherBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.i(Constants.RECEIVER_TAG, "Another receiver is working ");
            }
        };
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonStatic:
                //sendBroadCast(mEditTextData.getText().toString());

                // To make this work, you need to build another application with an activity called  MainActivity and broadcast receiver as below
                /*
                public class StartAppReciever  extends BroadcastReceiver {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                    Intent i = new Intent(context,MainActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity( i);
                    }
                }
                 In addition to register the receiver
                  <receiver android:name=".StartAppReciever"
                android:exported="true"
                android:enabled="true"
                android:description="@string/app_name"
                android:logo="@color/colorAccent"
                >
                    <intent-filter>
                        <action android:name="sdkjordan.com.alkaff.reciecerapp.START"></action>
                        <category android:name="android.intent.category.DEFAULT"></category>
                </intent-filter>
            </receiver>

                 */
                // this will send a global broadcast to do this action  sdkjordan.com.alkaff.reciecerapp.START
                Intent intent = new Intent("sdkjordan.com.alkaff.reciecerapp.START");
                sendBroadcast(intent);
                break;
        }
    }

    private void sendBroadCast(String data) {

        Intent intent = new Intent(RECIEVER_ACTION);
        intent.putExtra(Constants.Extras.EXTRA_DATA, data);

        // to send global broadcast
//        sendBroadcast(intent);

        // to send local broadcast
        localBroadcastManager.sendBroadcast(intent);
    }

    @Override
    protected void onPause() {
        if (mStaticReceiver != null)
            // to register global broadcast
//            unregisterReceiver(mStaticReceiver);

            // to unregister local broadcast
            localBroadcastManager.unregisterReceiver(mStaticReceiver);

        super.onPause();
    }

    @Override
    protected void onResume() {
        // to register global broadcast
//        registerReceiver(mStaticReceiver,mIntentFilter);


        // to register local broadcast
        localBroadcastManager.registerReceiver(mStaticReceiver, mIntentFilter);
        super.onResume();
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
        if (checked)
            // to register global broadcast
//            registerReceiver(anotherBroadcastReceiver,mIntentFilter);


            // to register local broadcast
            localBroadcastManager.registerReceiver(anotherBroadcastReceiver, mIntentFilter);
        else

            // to unregister global broadcast
//            unregisterReceiver(anotherBroadcastReceiver);


            // to unregister local broadcast
            localBroadcastManager.unregisterReceiver(anotherBroadcastReceiver);
    }
}
