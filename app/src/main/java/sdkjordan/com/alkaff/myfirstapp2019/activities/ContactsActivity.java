package sdkjordan.com.alkaff.myfirstapp2019.activities;

import android.Manifest;
import android.app.ListActivity;
import android.content.ContentResolver;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import sdkjordan.com.alkaff.myfirstapp2019.R;

public class ContactsActivity extends ListActivity {


    private static final int REQUEST_ID = 5;
    ContentResolver cr = null;
    private String[] from = {ContactsContract.Contacts._ID, ContactsContract.Contacts.DISPLAY_NAME};
    private int[] to = {R.id.textViewPhone, R.id.textViewName};

    public static final String KEY_ID = "_id";
    public static final String KEY_NAME = "name";
    public static final String KEY_PHONE = "phone";
    public static final String KEY_EMAIL = "email";
    private String[] keys = {KEY_NAME, KEY_PHONE, KEY_EMAIL};
    private int[] ids = {R.id.textViewName, R.id.textViewPhone, R.id.textViewEmail};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (checkContactPermissions()) {
            loadContacts();

        }

    }

    public List<Map<String, String>> getContacts() {

        List<Map<String, String>> contacts = new ArrayList<>();


        String phoneNumber = null;
        String email = null;

        Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
        String _ID = ContactsContract.Contacts._ID;
        String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
        String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;

        Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
        String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;

        Uri EmailCONTENT_URI = ContactsContract.CommonDataKinds.Email.CONTENT_URI;
        String EmailCONTACT_ID = ContactsContract.CommonDataKinds.Email.CONTACT_ID;
        String DATA = ContactsContract.CommonDataKinds.Email.DATA;

        StringBuffer output = new StringBuffer();

        ContentResolver contentResolver = getContentResolver();

        Cursor cursor = contentResolver.query(CONTENT_URI, null, null, null, null);

        // Loop for every contact in the phone
        if (cursor.getCount() > 0) {

            while (cursor.moveToNext()) {

                HashMap<String, String> map = new HashMap<>();
                map.put(KEY_ID, cursor.getString(cursor.getColumnIndex(_ID)));
                map.put(KEY_NAME, cursor.getString(cursor.getColumnIndex(DISPLAY_NAME)));

                int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(HAS_PHONE_NUMBER)));

                if (hasPhoneNumber > 0) {

                    // Query and loop for every phone number of the contact

                    Cursor phoneCursor = contentResolver.query(PhoneCONTENT_URI, null, Phone_CONTACT_ID + " = ?", new String[]{map.get(KEY_ID)}, null);

                    while (phoneCursor.moveToNext()) {
                        phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(NUMBER));
                        output.append(phoneNumber).append("/");
                    }
                    if(output.length() > 0)
                    map.put(KEY_PHONE, output.substring(0, output.length() - 1));
                    else
                        map.put(KEY_PHONE,"");

                                phoneCursor.close();

                    // Query and loop for every email of the contact
                    Cursor emailCursor = contentResolver.query(EmailCONTENT_URI, null, EmailCONTACT_ID + " = ?", new String[]{map.get(KEY_ID)}, null);

                    // clear the buffer
                    output.delete(0, output.length());

                    while (emailCursor.moveToNext()) {
                        email = emailCursor.getString(emailCursor.getColumnIndex(DATA));
                        output.append(email).append("/");
                    }

                    if(output.length() > 0)
                    map.put(KEY_EMAIL, output.substring(0, output.length() - 1));
                    else
                        map.put(KEY_EMAIL,"");
                    // clear the buffer
                    output.delete(0, output.length());

                    emailCursor.close();
                }
                contacts.add(map);
            }
        }
        return contacts;
    }


    private void loadContacts() {
        if (cr == null)
            cr = getContentResolver();
//        int id = 10 ;
//        int value  = 0 ;
////      content://com.android.contacts/people
//        Uri uri = ContactsContract.Contacts.CONTENT_URI;
//        String[] projection = from;         // null to get all the columns
//        String selection = null ; // ContactsContract.Contacts._ID + " = ? AND  "+ ContactsContract.Contacts._COUNT + " >  ?";            // null to select all rows
//        String[] selectionArgs = null ; //{String.valueOf(id), String.valueOf(value)};      // null because no selection
//        String sortedBy = null;              // null sorted by ID
//        Cursor c = cr.query(uri, projection, selection, selectionArgs, sortedBy);
//        setListAdapter(new SimpleCursorAdapter(ContactsActivity.this, R.layout.contact_list_item, c, from, to, SimpleAdapter.NO_SELECTION));


        setListAdapter(new SimpleAdapter(ContactsActivity.this,getContacts(), R.layout.contact_list_item,keys,ids));

    }

    private boolean checkContactPermissions() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(ContactsActivity.this, Manifest.permission.READ_CONTACTS)) {
                    Toast.makeText(ContactsActivity.this, "we need this permissions to read contacts", Toast.LENGTH_LONG).show();
                }
                requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, REQUEST_ID);
                return false;
            }
            return true;
        }
        return true;

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_ID) {
            for (int i : grantResults) {
                if (i != PackageManager.PERMISSION_GRANTED)
                    return;
            }
            loadContacts();
        }
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
    }
}