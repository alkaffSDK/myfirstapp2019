package sdkjordan.com.alkaff.myfirstapp2019.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import sdkjordan.com.alkaff.myfirstapp2019.R;
import sdkjordan.com.alkaff.myfirstapp2019.fragments.InformationFragment;
import sdkjordan.com.alkaff.myfirstapp2019.fragments.MyListFragment;
import sdkjordan.com.alkaff.myfirstapp2019.interfaces.DataSelector;

public class DynamicLayoutActivity extends AppCompatActivity implements DataSelector {

    public static String[] mCountryList;
    public static String[] mCountryInformation;

    private InformationFragment mInformationFragment;
    private FragmentManager  mFragmentManager;
    private FrameLayout mListFrameLayout, mInfoFrameLayout;

    private static final int MATCH_PARENT = LinearLayout.LayoutParams.MATCH_PARENT;
    private static final String TAG = "QuoteViewerActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, getClass().getSimpleName() + ":entered onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dynamic_acitivty);

        mCountryList = getResources().getStringArray(R.array.countries);
        mCountryInformation = getResources().getStringArray(R.array.countryInformation);



        mListFrameLayout =  findViewById(R.id.title_fragment_container);
        mInfoFrameLayout =  findViewById(R.id.info_fragment_container);


        MyListFragment myListFragment = new MyListFragment();
        mInformationFragment = new InformationFragment();
        //myListFragment.setData(mCountryList);

        mFragmentManager = getSupportFragmentManager();

        if(! myListFragment.isAdded())
        {
            FragmentTransaction fragmentTransaction = mFragmentManager
                    .beginTransaction();
            fragmentTransaction.add(R.id.title_fragment_container, myListFragment);
            fragmentTransaction.commit();
        }


        mFragmentManager
                .addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
                    public void onBackStackChanged() {
                        setLayout();
                    }
                });
    }

    private void setLayout() {
        if (!mInformationFragment.isAdded()) {
            mListFrameLayout.setLayoutParams(new LinearLayout.LayoutParams(
                    MATCH_PARENT, MATCH_PARENT));
            mInfoFrameLayout.setLayoutParams(new LinearLayout.LayoutParams(0,
                    MATCH_PARENT));
        } else {
            mListFrameLayout.setLayoutParams(new LinearLayout.LayoutParams(0,
                    MATCH_PARENT, 1f));
            mInfoFrameLayout.setLayoutParams(new LinearLayout.LayoutParams(0,
                    MATCH_PARENT, 2f));
        }
    }

    @Override
    protected void onDestroy() {
        Log.i(TAG, getClass().getSimpleName() + ":entered onDestroy()");
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        Log.i(TAG, getClass().getSimpleName() + ":entered onPause()");
        super.onPause();
    }

    @Override
    protected void onRestart() {
        Log.i(TAG, getClass().getSimpleName() + ":entered onRestart()");
        super.onRestart();
    }

    @Override
    protected void onResume() {
        Log.i(TAG, getClass().getSimpleName() + ":entered onResume()");
        super.onResume();
    }

    @Override
    protected void onStart() {
        Log.i(TAG, getClass().getSimpleName() + ":entered onStart()");
        super.onStart();
    }

    @Override
    protected void onStop() {
        Log.i(TAG, getClass().getSimpleName() + ":entered onStop()");
        super.onStop();
    }

    @Override
    public String[] getList() {
        return mCountryList;
    }

    public String getData(int i) {
        return mCountryList[i] ;
    }

    @Override
    public void selectChanged(int i) {
        if (!mInformationFragment.isAdded()) {
            FragmentTransaction fragmentTransaction = mFragmentManager
                    .beginTransaction();
            fragmentTransaction.add(R.id.info_fragment_container,
                    mInformationFragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
            mFragmentManager.executePendingTransactions();
        }
        mInformationFragment.onSelectTextChanged(mCountryInformation[i]);

    }
}
