package sdkjordan.com.alkaff.myfirstapp2019.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

import sdkjordan.com.alkaff.myfirstapp2019.R;

public class InternalFileWriteReadActivity extends AppCompatActivity implements View.OnClickListener {


    private final static String fileName = "chatFile.txt";


    private Button mButtonSend;
    private EditText mEditTextChat, mEditTextArea;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_internal_file_write_read);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        mButtonSend = findViewById(R.id.button_chatbox_send);
        mEditTextArea = findViewById(R.id.editTextArea);
        mEditTextChat = findViewById(R.id.edittext_chatbox);

        mButtonSend.setOnClickListener(this);

        readDataFromFile();
    }

    @Override
    public void onClick(View view) {
        String txt = mEditTextChat.getText().toString();
        if (txt != null && !txt.isEmpty()) {
            writeTextToFile(txt);
            mEditTextChat.setText("");
        }

        readDataFromFile();
    }

    private void readDataFromFile() {

        if (!getFileStreamPath(fileName).exists()) {
            mEditTextArea.setHint("No Data");
        } else {
            try (FileInputStream fis = openFileInput(fileName);
                 BufferedReader br = new BufferedReader(new InputStreamReader(fis))) {
                String line = "";
                while (null != (line = br.readLine()))
                    mEditTextArea.append("You:" + line + "\n");

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void writeTextToFile(String txt) {
        try (FileOutputStream fos = openFileOutput(fileName, MODE_PRIVATE);
             PrintWriter pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(fos)))) {
            pw.println(txt);
            pw.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void writeTextToPublicFile(String txt, String filepath) {
        File f = new File(filepath);
        if (f.exists()) {

            try (FileOutputStream fos = new FileOutputStream(f);
                 PrintWriter pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(fos)))) {
                pw.println(txt);
                pw.flush();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }
}
