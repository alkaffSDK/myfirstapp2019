package sdkjordan.com.alkaff.myfirstapp2019.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import sdkjordan.com.alkaff.myfirstapp2019.Constants;
import sdkjordan.com.alkaff.myfirstapp2019.R;

public class Main2Activity extends AppCompatActivity implements View.OnClickListener {


    private Button mButton1, mButton2, mButton3;
    private EditText mEditTextData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        mButton1 = findViewById(R.id.button1);
        mButton2 = findViewById(R.id.button2);
        mButton3 = findViewById(R.id.button3);
        mEditTextData = findViewById(R.id.editTextData);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.button1:
                Intent exIntent = new Intent(Constants.Actions.FirstAction);
                startActivity(exIntent);
                break;
            case R.id.button2:
                // check the API level to be 23 or above for using runtime permissions
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                {
                    // check if the permission was granted before
                    if(ActivityCompat.checkSelfPermission(getApplicationContext(),Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED ||
                            checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED )
                    {
                        // check if the permission was rejected before
                        if(ActivityCompat.shouldShowRequestPermissionRationale(Main2Activity.this,Manifest.permission.CALL_PHONE))
                            Toast.makeText(getApplicationContext(),"We need permission to call the phone",Toast.LENGTH_LONG).show();

                        if(ActivityCompat.shouldShowRequestPermissionRationale(Main2Activity.this,Manifest.permission.WRITE_EXTERNAL_STORAGE))
                            Toast.makeText(getApplicationContext(),"We don;t realy need this permission, it is just for fun",Toast.LENGTH_LONG).show();

                        // Ask the user for permission
                        ActivityCompat.requestPermissions(Main2Activity.this,new String[]{Manifest.permission.CALL_PHONE, Manifest.permission.WRITE_EXTERNAL_STORAGE},Constants.PERMISSION_REQUEST_CODE_CALL);
                        return;

                    }else {
                        callNumber(mEditTextData.getText().toString());
                    }
                }else{
                    callNumber(mEditTextData.getText().toString());
                }


                break;
            case R.id.button3:
                Intent exIntent3 = new Intent(Intent.ACTION_VIEW);
                String text = mEditTextData.getText().toString() ;
                if(! text.startsWith("http://"))
                    text = "http://" + text ;
                exIntent3.setData(Uri.parse(text) );
                startActivity(exIntent3);
                break;
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    super.onRequestPermissionsResult(requestCode,permissions,grantResults);
        if(requestCode == Constants.PERMISSION_REQUEST_CODE_CALL)
        {
            for(int i:grantResults)
            {
                if(i == PackageManager.PERMISSION_DENIED)
                    return;
            }

            callNumber(mEditTextData.getText().toString());
        }
    }

    private  void callNumber(String number)
    {
        Intent exIntent2 = new Intent(Intent.ACTION_CALL);
        exIntent2.setData(Uri.parse("tel:"+number)) ;
        startActivity(exIntent2);
    }
}
