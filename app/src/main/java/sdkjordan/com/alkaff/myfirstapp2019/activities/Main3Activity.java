package sdkjordan.com.alkaff.myfirstapp2019.activities;

import android.content.SharedPreferences;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.TextView;

import java.util.Random;

import sdkjordan.com.alkaff.myfirstapp2019.Constants;
import sdkjordan.com.alkaff.myfirstapp2019.R;

public class Main3Activity extends AppCompatActivity implements View.OnClickListener {

    private TextView mTextViewHighest, mTextViewScore ;

    private  int highest = 0 , score ;
    Random random = new Random();
    SharedPreferences mPreferences ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        mPreferences = getPreferences(MODE_PRIVATE);
        //mPreferences = getSharedPreferences("My_prefs", MODE_PRIVATE);

        init();


        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(this);

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        highest = mPreferences.getInt(Constants.Keys.HIGHEST_SCORE,0);
        mTextViewHighest.setText(String.valueOf(highest));
    }


    @Override
    protected void onPause() {
        super.onPause();
        saveToSharedPrefrences(highest);
    }

    private void init() {
        mTextViewHighest = findViewById(R.id.textViewHighest);
        mTextViewScore = findViewById(R.id.textViewScore);

    }

    @Override
    public void onClick(View view) {

        score = random.nextInt(1000);
        mTextViewScore.setText(String.valueOf(score));
        if(score >  highest) {
            highest = score;
            mTextViewHighest.setText(String.valueOf(highest));
            Snackbar.make(view, "You got a highest score: "+ highest, Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        }
    }

    private void saveToSharedPrefrences(int highest) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putInt(Constants.Keys.HIGHEST_SCORE,highest);

        editor.commit();
    }


}
