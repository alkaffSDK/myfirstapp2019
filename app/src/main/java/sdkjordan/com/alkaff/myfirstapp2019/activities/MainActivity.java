package sdkjordan.com.alkaff.myfirstapp2019.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.Random;

import sdkjordan.com.alkaff.myfirstapp2019.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    public static final String DATA_EXTRA ="data";

    private static final String TAG = "myfirstapp2019";
    private static final int MY_REQUEST_CODE =  10;
    private static  String ACTIVITY = "MainActivity";


    ConstraintLayout mContainer ;
    TextView mTextView ;
    TextView mTextViewResult ;

    Random rand = new Random();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG,String.format("%20s :%s",ACTIVITY,"onCreate()"));

//        TextView mTextView = new TextView(this);
//        LinearLayout.LayoutParams prams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT);
//        mTextView.setLayoutParams(prams);
//        mTextView.setOnClickListener(this);
//        mTextView.setText("Hello World!");


        mTextView = findViewById(R.id.textview);
        mTextView.setOnClickListener(this);

        mTextViewResult = findViewById(R.id.textViewResult);


        mContainer = findViewById(R.id.container);

    }

    public void onClick(View view) {

        Intent intent = new Intent(MainActivity.this,MyActivity.class) ;
        intent.putExtra(DATA_EXTRA,"this is the text") ;
//        startActivity(intent);
        startActivityForResult(intent,MY_REQUEST_CODE);
//        mContainer.setBackgroundColor(Color.rgb(rand.nextInt(255),rand.nextInt(255),rand.nextInt(255)));
//        Log.v(TAG,"onClick is called");
//        Log.d(TAG,"onClick is called");
//        Log.i(TAG,"onClick is called");
//        Log.w(TAG,"onClick is called");
//        Log.e(TAG,"onClick is called");
//        Log.wtf(TAG,"onClick is called");


    }


    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG,String.format("%20s :%s",ACTIVITY,"onStart()"));
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG,String.format("%20s :%s",ACTIVITY,"onStop()"));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG,String.format("%20s :%s",ACTIVITY,"onDestroy()"));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.d(TAG,String.format("%20s :%s",ACTIVITY,"onBackPressed()"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG,String.format("%20s :%s",ACTIVITY,"onPause()"));
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG,String.format("%20s :%s",ACTIVITY,"onResume()"));
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG,String.format("%20s :%s",ACTIVITY,"onRestart()"));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode == MY_REQUEST_CODE)
        {
            switch (resultCode)
            {
                case RESULT_CANCELED:
                    mTextViewResult.setText("The process was canceled!!");
                    break;
                case RESULT_OK:
                    if(data != null)
                    {
                        String text = data.getStringExtra(MainActivity.DATA_EXTRA);
                        mTextViewResult.setText(text);
                    }
                    break;
                    default:

            }
        }
    }
}
