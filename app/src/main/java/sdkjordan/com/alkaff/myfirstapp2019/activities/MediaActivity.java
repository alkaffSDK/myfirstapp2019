package sdkjordan.com.alkaff.myfirstapp2019.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.MediaController.MediaPlayerControl;

import android.os.Bundle;

import java.util.ArrayList;

import sdkjordan.com.alkaff.myfirstapp2019.adapters.SongAdapter;
import sdkjordan.com.alkaff.myfirstapp2019.R;
import sdkjordan.com.alkaff.myfirstapp2019.models.Song;
import sdkjordan.com.alkaff.myfirstapp2019.controllers.MusicController;
import sdkjordan.com.alkaff.myfirstapp2019.services.AnotherMediaPlayerService;

public class MediaActivity extends AppCompatActivity implements MediaPlayerControl, View.OnClickListener, AdapterView.OnItemClickListener {

    public static final String TAG = "MediaPlayerActivity";


    private ListView mListView;
    private SongAdapter setAdapter;
    private MusicController controller;
    private AnotherMediaPlayerService musicSrv;
    private ServiceConnection musicConnection ;
    private Intent playIntent;
    private ArrayList<Song> songList;
    private boolean musicBound=false;
    private boolean paused=false, playbackPaused=false;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_media);
        init();


    }

    private void init() {
        musicSrv = new AnotherMediaPlayerService();

        mListView = findViewById(R.id.music_list);
        mListView.setOnItemClickListener(this);

        // load the songs to the songs list, must be called before   setAdapter = new SongAdapter(this, songList);
        songList =  loadSongList();
        setAdapter = new SongAdapter(this, songList);
        mListView.setAdapter(setAdapter);


        //connect to the service
         musicConnection = new ServiceConnection(){

            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                AnotherMediaPlayerService.MusicBinder binder = (AnotherMediaPlayerService.MusicBinder)service;
                //get service
                musicSrv = binder.getService();
                //pass list
                musicSrv.setList(songList);
                musicBound = true;
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                musicBound = false;
            }
        };
    }


    @Override
    protected void onStart() {
        super.onStart();
        if(playIntent==null){
            playIntent = new Intent(this, AnotherMediaPlayerService.class);
            bindService(playIntent, musicConnection, Context.BIND_AUTO_CREATE);
            startService(playIntent);
            Log.i(TAG,"Service is bounded");
        }
    }




    private void setController() {

        Log.i(TAG,"setController");
        //set the controller up
        controller = new MusicController(this);

        controller.setPrevNextListeners(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playNext();
            }
        }, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playPrev();
            }
        });

    }


    //play next
    private void playNext(){
        musicSrv.playNext();
        if(playbackPaused){
            setController();
            playbackPaused=false;
        }
        controller.show(0);
    }

    //play previous
    private void playPrev(){
        musicSrv.playPrev();
        if(playbackPaused){
            setController();
            playbackPaused=false;
        }
        controller.show(0);
    }

    @Override
    public void start() {
        musicSrv.go();
    }

    @Override
    public void pause() {
        musicSrv.pausePlayer();

    }

    @Override
    public int getDuration() {
        return musicSrv.getDur();
    }

    @Override
    public int getCurrentPosition() {
        if (musicSrv != null && musicBound && musicSrv.isPng())
            return musicSrv.getDur();

        return 0;
    }

    @Override
    public void seekTo(int i) {
        musicSrv.seek(i);
    }

    @Override
    public boolean isPlaying() {
        if (musicSrv != null && musicBound)
            return musicSrv.isPng();
        return false;
    }

    @Override
    public int getBufferPercentage() {
        return 0;
    }

    @Override
    public boolean canPause() {
        return true;
    }

    @Override
    public boolean canSeekBackward() {
        return true;
    }

    @Override
    public boolean canSeekForward() {
        return true;
    }

    @Override
    public int getAudioSessionId() {
        if (musicSrv != null && musicBound && musicSrv.isPng())
            return musicSrv.getPosn();
        return 0;
    }

    public ArrayList<Song> loadSongList() {

        if (songList == null)
            songList = new ArrayList<>();
        if (songList.size() > 0)
            songList.clear();


        songList.add(new Song(R.raw.track01, "Track01", "SDK"));
        songList.add(new Song(R.raw.track02, "Track02", "SDK"));
        songList.add(new Song(R.raw.track03, "Track03", "SDK"));
        songList.add(new Song(R.raw.track04, "Track04", "SDK"));
        songList.add(new Song(R.raw.track05, "Track05", "SDK"));


        return songList;


    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //menu item selected
        switch (item.getItemId()) {
            case R.id.action_shuffle:
                //shuffle
                musicSrv.setShuffle();
                break;
            case R.id.action_end:
                stopService(playIntent);
                musicSrv=null;
                System.exit(0);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume(){
        super.onResume();
        if(paused){
            setController();
            paused=false;
        }
    }

    @Override
    protected void onPause(){
        super.onPause();
        playbackPaused=true;
        musicSrv.pausePlayer();
        paused=true;
    }

    @Override
    protected void onStop() {
        controller.hide();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        stopService(playIntent);
        musicSrv=null;
        super.onDestroy();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        musicSrv.setSong(i);
        musicSrv.playSong(i);
        if(playbackPaused){
            setController();
            playbackPaused=false;
        }
        if(controller != null)
            controller.show(0);
    }


}
