package sdkjordan.com.alkaff.myfirstapp2019.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import sdkjordan.com.alkaff.myfirstapp2019.R;
import sdkjordan.com.alkaff.myfirstapp2019.controllers.MusicController;
import sdkjordan.com.alkaff.myfirstapp2019.services.AnotherMediaPlayerService;
import sdkjordan.com.alkaff.myfirstapp2019.services.MusicService;

public class MusicServiceClient extends AppCompatActivity {


	MusicController controller  ;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_music_player);

		Intent intent = new Intent( getApplicationContext(), AnotherMediaPlayerService.class );
		intent.setAction( AnotherMediaPlayerService.ACTION_PLAY );
		startService( intent );


		// Intent used for starting the MusicService
		final Intent musicServiceIntent = new Intent(getApplicationContext(),
				MusicService.class);

		final Button startButton =  findViewById(R.id.start_button);
		startButton.setOnClickListener(new OnClickListener() {
			public void onClick(View src) {


				// Start the MusicService using the Intent
				startService(musicServiceIntent);

			}
		});

		final Button stopButton =  findViewById(R.id.stop_button);
		stopButton.setOnClickListener(new OnClickListener() {
			public void onClick(View src) {

				// Stop the MusicService using the Intent
				stopService(musicServiceIntent);

			}
		});

	}



}