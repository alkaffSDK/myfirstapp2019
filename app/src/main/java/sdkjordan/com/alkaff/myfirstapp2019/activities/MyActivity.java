package sdkjordan.com.alkaff.myfirstapp2019.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import sdkjordan.com.alkaff.myfirstapp2019.R;

public class MyActivity extends AppCompatActivity implements View.OnClickListener, View.OnLongClickListener {


    private static final String TAG = "myfirstapp2019";
    private static String ACTIVITY = "MyActivity";
    private String data;

    private TextView mTextView;
    private EditText mEditText;
    private Button mButton, mButtonA, mButtonB, mButtonC;

    private View.OnClickListener mOnClickListener;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_activity_layout);
        Log.d(TAG, String.format("%20s :%s", ACTIVITY, "onCreate()"));

        init();


        Intent intent = getIntent();
        data = intent.getStringExtra(MainActivity.DATA_EXTRA);

        if (data != null) {
            mTextView.setText(data);
            mEditText.setText(data);
        }

    }

    private void init() {
        mTextView = findViewById(R.id.textView2);
        mEditText = findViewById(R.id.editText);
        mButton = findViewById(R.id.button2);
        mButtonA = findViewById(R.id.buttonA);
        mButtonB = findViewById(R.id.buttonB);
        mButtonC = findViewById(R.id.buttonC);


        mOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()) {
                    case R.id.button2:
                        button2Clicked();
                        break;
                    case R.id.buttonA:
                        buttonAClicked();
                        break;
                    case R.id.buttonB:
                        buttonBClicked();
                        break;
                    case R.id.buttonC:
                        buttonCClicked();
                        break;
                }


            }
        };

//        mButton.setOnClickListener(mOnClickListener);
//        mButtonA.setOnClickListener(mOnClickListener);
//        mButtonB.setOnClickListener(mOnClickListener);
//        mButtonC.setOnClickListener(mOnClickListener);

        mButton.setOnClickListener(this);
        mButtonA.setOnClickListener(this);
        mButtonB.setOnClickListener(this);
        mButtonC.setOnClickListener(this);

        mButton.setOnLongClickListener(this);


    }

    private void buttonCClicked() {

    }

    private void buttonBClicked() {
    }


    private void buttonAClicked() {

    }

    private void button2Clicked() {
//        Intent intent = new Intent(MyActivity.this,MainActivity.class);
//        startActivity(intent);

        String text = mEditText.getText().toString();
        if( text ==  null || text.isEmpty() )
            setResult(RESULT_CANCELED);
        else {
            Intent intent = new Intent();
            intent.putExtra(MainActivity.DATA_EXTRA,text);

            setResult(RESULT_OK,intent);
        }
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, String.format("%20s :%s", ACTIVITY, "onStart()"));
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, String.format("%20s :%s", ACTIVITY, "onStop()"));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, String.format("%20s :%s", ACTIVITY, "onDestroy()"));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.d(TAG, String.format("%20s :%s", ACTIVITY, "onBackPressed()"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, String.format("%20s :%s", ACTIVITY, "onPause()"));
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, String.format("%20s :%s", ACTIVITY, "onResume()"));
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, String.format("%20s :%s", ACTIVITY, "onRestart()"));
    }

    public void clickMe(View v) {
        // TODO: Add your code here

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button2:
                button2Clicked();
                break;
            case R.id.buttonA:
                buttonAClicked();
                break;
            case R.id.buttonB:
                buttonBClicked();
                break;
            case R.id.buttonC:
                buttonCClicked();
                break;
        }
    }

    @Override
    public boolean onLongClick(View view) {
        return false;
    }
}
