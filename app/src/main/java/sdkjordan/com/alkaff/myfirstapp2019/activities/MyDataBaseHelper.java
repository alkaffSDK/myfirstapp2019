package sdkjordan.com.alkaff.myfirstapp2019.activities;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import sdkjordan.com.alkaff.myfirstapp2019.Constants;

public class MyDataBaseHelper extends SQLiteOpenHelper {

    public static final int VERSION = 1 ;
    public MyDataBaseHelper(@Nullable Context context) {
        super(context, Constants.DataBase.DB_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE  "+  Constants.DataBase.TABLE_NAME+" (" +
                Constants.DataBase.COLUMN_ID+"  INTEGER PRIMARY KEY autoincrement," +
                Constants.DataBase.COLUMN_NAME+"  TEXT NOT NULL," +
                Constants.DataBase.COLUMN_PHONE+"  TEXT NOT NULL UNIQUE" +
                ");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        // TODO: change the scheme to the new db and move the old data if any

    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO: change the scheme to the old db and move the old data if any
    }
}
