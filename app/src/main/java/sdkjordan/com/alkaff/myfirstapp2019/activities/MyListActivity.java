package sdkjordan.com.alkaff.myfirstapp2019.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import sdkjordan.com.alkaff.myfirstapp2019.Constants;
import sdkjordan.com.alkaff.myfirstapp2019.R;

public class MyListActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {


    private static final String TAG = "MyListActivity";
    private static String[] from = {Constants.Keys.NAME, Constants.Keys.PHONE};
    private static int[] to = {R.id.textView, R.id.textView3};

    private String[] Data = {"A", "B", "C", "D"};
    private ListView mListView;

    private Button mButtonAdd;
    private ArrayAdapter<String> arrayAdapter;  // to read from array of strings
    private SimpleAdapter simpleAdapter;        // to read from List of maps
    SimpleCursorAdapter cursorAdapter;         // to read from DB

    private EditText mEditTextName, mEditTextPhone;
    private List<HashMap<String, String>> data;

    MyDataBaseHelper myDataBaseHelper;
    SQLiteDatabase mDatabase;
    MySharedPreferences preferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        data = new ArrayList<>();

        preferences = new MySharedPreferences(getApplicationContext());

        myDataBaseHelper = new MyDataBaseHelper(getApplicationContext());

        init();
    }

    private void init() {

        mListView = findViewById(R.id.listview);
        mButtonAdd = findViewById(R.id.buttonAdd);
        mEditTextName = findViewById(R.id.editTextName);
        mEditTextPhone = findViewById(R.id.editTextPhone);

        mButtonAdd.setOnClickListener(this);

        mListView.setOnItemClickListener(this);
        mListView.setOnItemLongClickListener(this);

//        Data = getResources().getStringArray(R.array.countries);
//        Arrays.sort(Data);
//        arrayAdapter = new ArrayAdapter<String>(getApplicationContext(),R.layout.list_item_layout,R.id.textView,Data);
//        mListView.setAdapter(arrayAdapter);


        data = preferences.getContacts();

//        simpleAdapter = new SimpleAdapter(getApplicationContext(), data, R.layout.list_item_layout, from, to);
//        mListView.setAdapter(simpleAdapter);


        String[] fromDB = {Constants.DataBase.COLUMN_PHONE, Constants.DataBase.COLUMN_NAME};
        cursorAdapter = new SimpleCursorAdapter(getApplicationContext(), R.layout.list_item_layout, getContactsFromDB(), fromDB, to, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);

        mListView.setAdapter(cursorAdapter);
    }

    private Cursor getContactsFromDB() {
        String query = "SELECT * FROM " + Constants.DataBase.TABLE_NAME + ";";
        return myDataBaseHelper.getReadableDatabase().rawQuery(query, null);
    }

    // ' OR '1' = '1
    private Cursor searchContactsFromDBByName(String name) {
        // another way (bad way)
//        String query = String.format( "SELECT * FROM %s WHERE %s like \'%%%s%%\' ; ", Constants.DataBase.TABLE_NAME ,Constants.DataBase.COLUMN_NAME,name);
//        return myDataBaseHelper.getReadableDatabase().rawQuery(query, null);
//
//         another way (bad way)

//        String query = String.format( "SELECT * FROM %s WHERE %s like \'%%?%%\' ; ", Constants.DataBase.TABLE_NAME ,Constants.DataBase.COLUMN_NAME);
//        return myDataBaseHelper.getReadableDatabase().rawQuery(query, new String[]{name});

        String[] columns = {Constants.DataBase.COLUMN_ID, Constants.DataBase.COLUMN_PHONE, Constants.DataBase.COLUMN_NAME}; // or null for all columns
        String selection = Constants.DataBase.COLUMN_NAME + " LIKE \'%?%\' ";                // what comes after the where clause, without the where
        String[] selectionArgs = {name};
        Cursor c = myDataBaseHelper.getReadableDatabase().query(Constants.DataBase.TABLE_NAME, columns, selection, selectionArgs, null, null, null, null);

        return c;
    }

    @Override
    public void onClick(View view) {

        InsertDummyData();
        cursorAdapter.changeCursor(getContactsFromDB());
//        String name = mEditTextName.getText().toString();
//        String phone = mEditTextPhone.getText().toString();
//        if (name.length() > 3 && phone.length() > 7) {
//            HashMap<String, String> map = new HashMap<>();
//            map.put(Constants.Keys.NAME, name);
//            map.put(Constants.Keys.PHONE, phone);
//            data.add(map);
//
//
//            simpleAdapter.notifyDataSetChanged();
//            clearText();
//        }

    }


    @Override
    protected void onResume() {
        super.onResume();
//         data = preferences.getContacts();
//        simpleAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onStop() {
        preferences.saveContacts(data);

        saveToFileJava7(data);
        //saveToDatabase(data);

        super.onStop();

    }

    private void saveToDatabase(List<HashMap<String, String>> data) {

        try (SQLiteDatabase mdb = myDataBaseHelper.getWritableDatabase()) {
            long rowID;
            ContentValues values;
            for (HashMap<String, String> map : data) {
                values = new ContentValues();
                for (String k : map.keySet())
                    values.put(k, map.get(k));
                rowID = mdb.insert(Constants.DataBase.TABLE_NAME, null, values);

                if (rowID == -1)
                    Log.e(Constants.TAG, "Error inserting data : " + values);
                else
                    Log.i(Constants.TAG, "A new row was inserting :" + values);

            }
        }
    }


    private int getNewID() {
        try (SQLiteDatabase mdb = myDataBaseHelper.getReadableDatabase()) {
            Cursor c = mdb.rawQuery(String.format("SELECT MAX(%s) FROM %s ; ", Constants.DataBase.COLUMN_ID, Constants.DataBase.TABLE_NAME), null);
            if (c.moveToFirst())
                return c.getInt(0) + 1;
        }
        return 1;
    }


    private void InsertDummyData() {

        mDatabase = myDataBaseHelper.getWritableDatabase();
        long rowID;
        int id;
        try (SQLiteDatabase mdb = myDataBaseHelper.getWritableDatabase()) {
            // Read the max id
//            Cursor c =  mdb.rawQuery(String.format("SELECT MAX(%s) FROM %s ; ",Constants.DataBase.COLUMN_ID, Constants.DataBase.TABLE_NAME),null) ;
//            if(c.moveToFirst())
//                id =  c.getInt(0) + 1 ;
//            else
//                id = 1 ;


            // insert the data
            ContentValues v = new ContentValues();
//            v.put(Constants.DataBase.COLUMN_ID, id);
            v.put(Constants.DataBase.COLUMN_NAME, mEditTextName.getText().toString());
            v.put(Constants.DataBase.COLUMN_PHONE, mEditTextPhone.getText().toString());
            rowID = mDatabase.insert(Constants.DataBase.TABLE_NAME, null, v);


            if (rowID == -1)
                Log.e(Constants.TAG, "Error inserting data : " + v);
            else {
                Log.i(Constants.TAG, "A new row was inserting :" + v);
                clearText();
            }
        }

    }

    private void saveToFile(List<HashMap<String, String>> data) {

        FileOutputStream fos = null;
        PrintWriter pr = null;
        try {
            fos = openFileOutput(Constants.DATA_FILE_NAME, MODE_APPEND);
//            FileInputStream fis = openFileInput(Constants.DATA_FILE_NAME) ;
//            BufferedReader br = new BufferedReader(new InputStreamReader(fis));
            pr = new PrintWriter(fos);

            for (int i = 0; i < data.size(); i++) {
                pr.println(String.format("%15s:%d. %s (%s)"
                        , Calendar.getInstance().getTime(),
                        i,
                        data.get(i).get(Constants.Keys.NAME),
                        data.get(i).get(Constants.Keys.PHONE)));
            }

            pr.flush();
        } catch (FileNotFoundException e) {
            Log.e(Constants.TAG, e.getStackTrace().toString());
        } finally {

            try {
                if (fos != null) fos.close();
                if (pr != null) pr.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }

    private void saveToFileJava7(List<HashMap<String, String>> data) {

        try (FileOutputStream fos = openFileOutput(Constants.DATA_FILE_NAME, MODE_APPEND);
             PrintWriter pr = new PrintWriter(fos)) {
            for (int i = 0; i < data.size(); i++) {
                pr.println(String.format("%15s:%d. %s (%s)"
                        , Calendar.getInstance().getTime(),
                        i,
                        data.get(i).get(Constants.Keys.NAME),
                        data.get(i).get(Constants.Keys.PHONE)));
            }

            pr.flush();
        } catch (Exception e) {
            Log.e(Constants.TAG, e.getStackTrace().toString());
        }

    }

    private void clearText() {
        mEditTextPhone.setText("");
        mEditTextName.setText("");
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//        Toast.makeText(getApplicationContext(),"onClick",Toast.LENGTH_SHORT).show();

        TextView textView = view.findViewById(R.id.textView);
        String name = textView.getText().toString();
        String phone = data.get(i).get(Constants.Keys.PHONE);
        Toast.makeText(getApplicationContext(), String.format("%-20s:%10s", name, phone), Toast.LENGTH_SHORT).show();

    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
        data.remove(i);

        preferences.removeContact(i);
        simpleAdapter.notifyDataSetChanged();
        return true;
    }
}
