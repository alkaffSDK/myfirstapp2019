package sdkjordan.com.alkaff.myfirstapp2019.activities;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import sdkjordan.com.alkaff.myfirstapp2019.Constants;

public class MySharedPreferences {

    private SharedPreferences preferences ;
    public    MySharedPreferences(Context mContext)
    {
        preferences =  mContext.getSharedPreferences("my_prefs",Context.MODE_PRIVATE); ;
    }

    public int getHighestScore(){ return preferences.getInt(Constants.Keys.HIGHEST_SCORE,0);}

    public  void setHighestScore(int h)
    {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(Constants.Keys.HIGHEST_SCORE,h);
        editor.commit();
    }

    public void AddContacts(int id,String name, String phone)
    {
        Set<String> contact = preferences.getStringSet(Constants.Keys.CONTACTS,new HashSet<String>());
        contact.add(id+":"+name+":"+phone);

        storeToPrefs(contact);

    }

    public  boolean removeContact(int id)
    {
        Set<String> contact = preferences.getStringSet(Constants.Keys.CONTACTS,null);
        if(contact == null) return  false ;

        for (String s: contact) {
            if(s.startsWith(id+":"))
            {
                contact.remove(s);
                storeToPrefs(contact);
                return  true ;
            }
        }
        return  false ;

    }

    private void storeToPrefs(Set<String> contact) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(Constants.Keys.CONTACTS);
        editor.putStringSet(Constants.Keys.CONTACTS,contact);
        editor.commit();
    }

    public List<HashMap<String,String>> getContacts()
    {
        List<HashMap<String,String>> contacts = new ArrayList<>();
        Set<String> contact = preferences.getStringSet(Constants.Keys.CONTACTS,null);
        if(contact == null)
            return  new ArrayList<>();


        HashMap<String,String> map ;
        String[] strings ;
        for (String s:contact)
        {
            strings = s.split(":");
            map = new HashMap<>() ;
            map.put(Constants.Keys.ID,strings[0]);
            map.put(Constants.Keys.NAME,strings[1]);
            map.put(Constants.Keys.PHONE,strings[2]);

            contacts.add(map);
        }

        return  contacts ;
    }

    public void saveContacts(List<HashMap<String, String>> data) {

        HashSet<String> set = new HashSet<>();
        for(int i=0;i<data.size();i++)
        {
            set.add(i+":"+data.get(i).get(Constants.Keys.NAME)+":"+data.get(i).get(Constants.Keys.PHONE));
        }

        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(Constants.Keys.CONTACTS);
        editor.putStringSet(Constants.Keys.CONTACTS,set);
        editor.commit();
    }
}
