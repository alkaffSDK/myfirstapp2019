package sdkjordan.com.alkaff.myfirstapp2019.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import sdkjordan.com.alkaff.myfirstapp2019.R;

public class NotificationActivity extends AppCompatActivity implements View.OnClickListener, AlertDialog.OnClickListener {

    private static final String CHANNEL_ID = "channel_1";
    private static final int MY_PENNDING_REQUEST = 10;
    private EditText mEditTextToast;
    private EditText editText;
    private TextView mTextViewName;
    private EditText mEditTextCountry;


    private boolean isBackpressed = false;

    AlertDialog exitDialog;
    AlertDialog NameDialog;
    AlertDialog CountriesDialog;

    Notification notification;
    NotificationManager notificationManager;


    int selectedCountry = -1;
    String[] countries;


    AlertDialog.OnClickListener onClickListener;
    private int notification_id = 5;

    private Intent intent = new Intent("sdkjordan.com.alkaff.myfirstapp2019.FirstAction");
    private PendingIntent mpendingItent;
    private boolean[] checked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);


        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        createNotificationChannel();


        mpendingItent = PendingIntent.getActivity(getApplicationContext(), MY_PENNDING_REQUEST, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        init();
    }

    private void init() {
        mEditTextToast = findViewById(R.id.editTextToast);
        mTextViewName = findViewById(R.id.textViewName);
        mTextViewName.setOnClickListener(this);
        editText = new EditText(getApplicationContext());
        editText.setTextColor(Color.BLUE);

        mEditTextCountry = findViewById(R.id.editTextCountry);
        mEditTextCountry.setOnClickListener(this);

        countries = getResources().getStringArray(R.array.countries);

        checked = new boolean[countries.length];

        onClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                switch (i) {
                    case AlertDialog.BUTTON_POSITIVE:
                        if (dialogInterface.equals(NameDialog)) {
                            if (editText != null && !editText.getText().toString().isEmpty())
                                mTextViewName.setText(editText.getText().toString());
                        } else if (dialogInterface.equals(CountriesDialog)) {
//                            mEditTextCountry.setText(selectedCountry + ":" + getResources().getStringArray(R.array.countries)[selectedCountry]);
                            StringBuilder builder = new StringBuilder();

                            for (int j = 0; j < checked.length; j++) {

                                if (checked[j]) {
                                    builder.append(countries[j]).append(", ") ;
                                }
                            }
                            mEditTextCountry.setText(builder.substring(0,builder.length() - 2).toString());

                        }
                        break;
                    case AlertDialog.BUTTON_NEGATIVE:
                        dialogInterface.dismiss();
                        break;
                }
            }
        };
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonCustomToast:
                createCustomToast(mEditTextToast.getText().toString());
                mEditTextToast.setText("");
                break;
            case R.id.textViewName:
                showNameDialog();
                break;
            case R.id.editTextCountry:
                showCountriesDialog();
                break;
            case R.id.buttonNotification:
                if (null == notification) {
                    notification = createNotification();
                }
                notificationManager.notify(notification_id++, notification);
                break;
        }
    }


    private void showCountriesDialog() {


        if (null == CountriesDialog) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            CountriesDialog = builder.setTitle("Country")
//                    .setSingleChoiceItems(R.array.countries, -1, new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int i) {
//                            selectedCountry = i;
//                        }
//                    })
                    .setMultiChoiceItems(R.array.countries, checked, new DialogInterface.OnMultiChoiceClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i, boolean b) {
                            checked[i] = b;
                        }
                    })
                    .setCancelable(false)
                    .setPositiveButton("OK", onClickListener)
                    .setNegativeButton("Cancel", onClickListener).create();

        }
        CountriesDialog.show();

    }


    private void showPopupMenu(View v) {
        PopupMenu popupMenu = new PopupMenu(this, v);
        MenuInflater menuInflater = popupMenu.getMenuInflater();
        menuInflater.inflate(R.menu.popup_menu, popupMenu.getMenu());
        popupMenu.show();
    }

    private void showSnackBar(@NonNull View v, @NonNull String text, @Nullable String action, @Nullable View.OnClickListener listener) {
        Snackbar mySnackbar = Snackbar.make(v, text, Snackbar.LENGTH_SHORT);
        if (action != null)
            mySnackbar.setAction(action, listener);
        mySnackbar.show();

    }

    private void showNameDialog() {

        if (NameDialog == null) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            NameDialog = builder.setTitle("Enter your name")
                    .setCancelable(false)
                    .setView(editText)
                    .setPositiveButton("OK", onClickListener)
                    .setNegativeButton("Cancel", onClickListener).create();
        }
        NameDialog.show();
    }


    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        switch (i) {
            case AlertDialog.BUTTON_POSITIVE:
                super.onBackPressed();
                break;
            case AlertDialog.BUTTON_NEGATIVE:
                dialogInterface.dismiss();
                break;
            case AlertDialog.BUTTON_NEUTRAL:
                dialogInterface.dismiss();
                break;
        }

    }

    @Override
    public void onBackPressed() {

//        if(isBackpressed)
//        {
//            super.onBackPressed();
//        }else {
//            isBackpressed = true ;
//            Toast.makeText(getApplicationContext(),"Press back again to exit.",Toast.LENGTH_LONG).show();
//        }

        createExitDialog();
    }


    private void createExitDialog() {
//        builder.setTitle(getResources().getString(R.string.exit_txt));
//        builder.setMessage(getResources().getString(R.string.exit_msg)) ;
//        builder.setPositiveButton(R.string.yes_txt,this);
//        builder.setNegativeButton(R.string.no_txt,this);
//        builder.setCancelable(false);
//        builder.create().show();

        if (exitDialog == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            exitDialog = builder.setTitle(getResources().getString(R.string.exit_txt))
                    .setMessage(getResources().getString(R.string.exit_msg))
                    .setPositiveButton(R.string.yes_txt, this)
                    .setNegativeButton(R.string.no_txt, this)
                    .setCancelable(false)
                    .create();
        }
        exitDialog.show();

    }

    private void createCustomToast(String text) {

        Toast toast = new Toast(getApplicationContext());
        toast.setDuration(Toast.LENGTH_LONG);
        View v = getLayoutInflater().inflate(R.layout.cutom_toast, null);
        TextView textView = v.findViewById(R.id.textViewToast);
        textView.setText(text);
        toast.setView(v);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }


    private Notification createNotification() {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID);
        builder.setSmallIcon(android.R.drawable.ic_dialog_alert)
                .setContentTitle("Notification")
                .setContentIntent(mpendingItent)
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentText("This is the text of the notification");

        return builder.build();
    }


    private void createNotificationChannel() {

        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (null == notificationManager.getNotificationChannel(CHANNEL_ID)) {
                CharSequence name = getString(R.string.channel_name);
                String description = getString(R.string.channel_description);
                int importance = NotificationManager.IMPORTANCE_DEFAULT;
                NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
                channel.setDescription(description);
                // Register the channel with the system; you can't change the importance
                // or other notification behaviors after this
                notificationManager = getSystemService(NotificationManager.class);
                notificationManager.createNotificationChannel(channel);
            }
        }
    }

}