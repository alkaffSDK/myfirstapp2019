package sdkjordan.com.alkaff.myfirstapp2019.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;

import sdkjordan.com.alkaff.myfirstapp2019.R;
import sdkjordan.com.alkaff.myfirstapp2019.fragments.InformationFragment;
import sdkjordan.com.alkaff.myfirstapp2019.interfaces.DataSelector;

public class StaticFragmentActivity extends AppCompatActivity implements DataSelector {

    private String[] countries;
    InformationFragment mDetailsFragment ;
    FragmentManager fragmentManager ;

    String[] information ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_static_fragment);

        countries = getResources().getStringArray(R.array.countries);
        information = getResources().getStringArray(R.array.countryInformation);

        fragmentManager = getSupportFragmentManager() ;
        mDetailsFragment = (InformationFragment) fragmentManager.findFragmentById(R.id.fragmentDetails);
    }

    @Override
    public String[] getList()
    {
        return countries;
    }

    @Override
    public void selectChanged(int i) {
        mDetailsFragment.onSelectTextChanged(information[i]);

    }
}
