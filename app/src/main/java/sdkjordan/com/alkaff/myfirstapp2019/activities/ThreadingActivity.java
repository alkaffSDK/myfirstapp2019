package sdkjordan.com.alkaff.myfirstapp2019.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import sdkjordan.com.alkaff.myfirstapp2019.R;

public class ThreadingActivity extends AppCompatActivity implements View.OnClickListener {

    private static final long DELAY = 1000;
    ImageView mImageView;
    ProgressBar mProgressBar;
    EditText mEditTextUrl;
    ProgressDialog dialog;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_threading);

        mImageView = findViewById(R.id.imageView3);
        mProgressBar = findViewById(R.id.progressBar);
        mProgressBar.setMax(100);
        dialog = new ProgressDialog(getApplicationContext());
        dialog.setMessage("Loading");
        dialog.setCancelable(false);
        mEditTextUrl = findViewById(R.id.editTextUrl);
    }

    int counter = 0;

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonLoad:
//                loadImage();
//                loadImageWithSleep();
//                loadImageWithSleepAndThreading();
                int id = R.raw.android;
                if (!isRunning)
                    if (mEditTextUrl.getText().toString().isEmpty()) {
                        switch (counter++ % 4) {
                            case 0:
                                id = R.raw.android;
                                break;
                            case 1:
                                id = R.raw.android1;
                                break;
                            case 2:
                                id = R.raw.android2;
                                break;
                            case 3:
                                id = R.raw.android3;
                                break;
                        }
                        new ImageLoader().execute(id);
                    } else {
                        new DownLoadImage(ThreadingActivity.this).execute(mEditTextUrl.getText().toString());
                    }
                break;
            case R.id.floatingActionButtonToast:
                Toast.makeText(this, Thread.currentThread().getName() + ": I'm working !!!", Toast.LENGTH_LONG).show();
                break;
        }
    }

    private void loadImage() {

        Bitmap img = BitmapFactory.decodeResource(getResources(), R.raw.android);
        mImageView.setImageBitmap(img);

    }

    private void loadImageWithSleep() {

        Bitmap img = BitmapFactory.decodeResource(getResources(), R.raw.android);
        sleep();
        mImageView.setImageBitmap(img);

    }

    // to stop running more than one thread at the same time
    boolean isRunning;

    private void loadImageWithSleepAndThreading() {

        if (!isRunning) {
            // Note: In Android it is not recommended to use Java Thread class
            new Thread(new Runnable() {
                @Override
                public void run() {
                    isRunning = true;
                    mProgressBar.setProgress(0);
                    final Bitmap img = BitmapFactory.decodeResource(getResources(), R.raw.android);
                    sleep();
                    // mImageView.setImageBitmap(img);     // this line will create an exception : Only the original thread that created a view hierarchy can touch its views.

                    // to overcome this problem you have more than option

                    // Option 1 : using post method to return the execute to the thread that created the view (original thread)

//                    mImageView.post(new Runnable() {
//                        @Override
//                        public void run() {
//                            mImageView.setImageBitmap(img);
//                            mProgressBar.setProgress(0);
//                            isRunning =false ;
//
//                        }
//                    });


                    // Option 2 : using runOnUiThread method to return the execute to the thread that main UI thread if there is no view.

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mImageView.setImageBitmap(img);
                            mProgressBar.setProgress(0);
                            isRunning = false;

                        }
                    });
                }
            }, "Worker thread").start();


        } else {
            Toast.makeText(this, "Wait... ", Toast.LENGTH_LONG).show();
        }
    }

    private void sleep() {
        try {
            for (int i = 1; i <= 100; i++) {
                Thread.sleep(DELAY / 100);
                mProgressBar.setProgress(i);
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    class ImageLoader extends AsyncTask<Integer, Float, Bitmap> {

        @Override
        protected void onPreExecute() {         // Runs in the Main Ui Thread
            mProgressBar.setProgress(0);
            isRunning = true;
        }


        @Override
        protected Bitmap doInBackground(Integer... integers) {          // Runs in the Worker Ui Thread


            try {
                for (int i = 1; i <= 100; i++) {
                    Thread.sleep(DELAY / 100);

                    // this will call onProgressUpdate
                    publishProgress(i / 100.0F);
                }


            } catch (InterruptedException e) {
                e.printStackTrace();
            }


            return BitmapFactory.decodeResource(getResources(), integers[0]);
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {               // Runs in the Main Ui Thread

            mImageView.setImageBitmap(bitmap);
            mProgressBar.setProgress(0);
            isRunning = false;
        }


        @Override
        protected void onProgressUpdate(Float... values) {              // Runs in the Worker Ui Thread
            mProgressBar.setProgress((int) (values[0] * 100));
        }

        @Override
        protected void onCancelled(Bitmap bitmap) {                     // Runs in the Worker Ui Thread
            mImageView.setImageBitmap(bitmap);
            mProgressBar.setProgress(0);
        }

        @Override
        protected void onCancelled() {
            mImageView.setImageBitmap(BitmapFactory.decodeResource(getResources(), android.R.drawable.ic_dialog_alert));
            mProgressBar.setProgress(0);
        }


    }

    private class DownLoadImage extends AsyncTask<String, Void, Bitmap> {

        Context mContext;
        ProgressDialog dialog ;

        public DownLoadImage(Context context) {
            mContext = context;
        }

        @Override
        protected void onPreExecute() {         // Runs in the Main Ui Thread
//           dialog = ProgressDialog.show(mContext, "", "Loading...", true);
            dialog = ProgressDialog.show(mContext,"Loading","LDownloading image", true,true);
            dialog.show();
        }


        @Override
        protected Bitmap doInBackground(String... prams) {          // Runs in the Worker Ui Thread

            try {
                URL url = new URL(prams[0]);
                HttpURLConnection connection = (HttpURLConnection) url
                        .openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(input);

                return getResizedBitmap(myBitmap, mImageView.getMaxHeight(), mImageView.getMaxWidth());
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }

        }
        // this method will reside the image to specified size newHeight x newWidth
        public Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
            int width = bm.getWidth();
            int height = bm.getHeight();
            float scaleWidth = ((float) newWidth) / width;
            float scaleHeight = ((float) newHeight) / height;
            // CREATE A MATRIX FOR THE MANIPULATION
            Matrix matrix = new Matrix();
            // RESIZE THE BIT MAP
            matrix.postScale(scaleWidth, scaleHeight);

            // "RECREATE" THE NEW BITMAP
            Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height,
                    matrix, false);

            return resizedBitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {               // Runs in the Main Ui Thread

            mImageView.setImageBitmap(bitmap);
            if (dialog.isShowing()) {
                dialog.dismiss();
            }

        }


        @Override
        protected void onCancelled(Bitmap bitmap) {                     // Runs in the Worker Ui Thread
            if (bitmap != null)
                mImageView.setImageBitmap(bitmap);
            else
                mImageView.setImageBitmap(BitmapFactory.decodeResource(mContext.getResources(), android.R.drawable.ic_dialog_alert));


            if (dialog.isShowing()) {
                dialog.dismiss();
            }

        }

        @Override
        protected void onCancelled() {
            mImageView.setImageBitmap(BitmapFactory.decodeResource(mContext.getResources(), android.R.drawable.ic_dialog_alert));
            if (dialog.isShowing()) {
                dialog.dismiss();
            }

        }
    }
}
