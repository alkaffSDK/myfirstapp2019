package sdkjordan.com.alkaff.myfirstapp2019.controllers;

import android.content.Context;
import android.widget.MediaController;


/**
 * The MediaController class presents a standard widget with play/pause, rewind, fast-forward, and skip (previous/next) buttons in it
 */
public class MusicController extends MediaController {


    public MusicController(Context c){
        super(c);
    }

    public void hide(){}
}
