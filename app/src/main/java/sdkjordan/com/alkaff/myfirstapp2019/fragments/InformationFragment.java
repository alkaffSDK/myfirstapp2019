package sdkjordan.com.alkaff.myfirstapp2019.fragments;

import android.content.Context;
import android.os.Bundle;
import android.provider.UserDictionary;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import sdkjordan.com.alkaff.myfirstapp2019.R;

public class InformationFragment extends Fragment {

    public static final String TAG = "fragments Details";

    private View mvView;
    private  int selectedIndex = -1 ;
    private String mLastSelectedText;
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Log.d(TAG,"onAttach");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG,"onCreate");

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG,"onCreateView");

        if (mvView == null) {
            mvView = inflater.inflate(R.layout.details_fragment,null);
        } else {
//            ViewGroup parent = (ViewGroup) mvView.getParent();
//            if (parent != null) {
//                parent.removeView(mvView);
//            }
        }
        return mvView;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG,"onActivityCreated");


        // this method will return the activity that host this fragments
        getActivity() ;
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG,"onStart");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG,"onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG,"onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG,"onStop");
    }

    @Override
    public void onDestroyView() {
        if (mvView != null) {
            ViewGroup parentViewGroup = (ViewGroup) mvView.getParent();
            parentViewGroup.removeAllViews();
        }
        super.onDestroyView();
        Log.d(TAG,"onDestroyView");

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG,"onDestroy");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG,"onDetach");
    }


    public void onSelectTextChanged(String text)
    {
        String[] info = text.split(",");
        if(mLastSelectedText != text)
        {
            TextView textView = getView().findViewById(R.id.textViewDetails);
            textView.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
            textView.setText(String.format("\tCapital : %s %n\tCode : %s %n\tContinent : %s",info[0],info[1],info[2]));
            mLastSelectedText = text ;

        }
    }
    
}
