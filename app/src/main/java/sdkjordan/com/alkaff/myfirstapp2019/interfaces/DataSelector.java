package sdkjordan.com.alkaff.myfirstapp2019.interfaces;

public interface DataSelector {
    String[] getList();

    void selectChanged(int i);
}
