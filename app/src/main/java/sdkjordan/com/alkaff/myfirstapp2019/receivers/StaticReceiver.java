package sdkjordan.com.alkaff.myfirstapp2019.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import sdkjordan.com.alkaff.myfirstapp2019.Constants;

public class StaticReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        String data   = null;
        if(intent != null)
        {
            data = intent.getStringExtra(Constants.Extras.EXTRA_DATA);
        }

        Log.i(Constants.RECEIVER_TAG, "Static receiver is working Data:"+data) ;
    }
}
