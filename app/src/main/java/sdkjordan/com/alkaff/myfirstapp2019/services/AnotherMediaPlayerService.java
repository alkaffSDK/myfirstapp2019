package sdkjordan.com.alkaff.myfirstapp2019.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.Rating;
import android.media.session.MediaController;
import android.media.session.MediaSession;
import android.media.session.MediaSessionManager;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.Random;

import sdkjordan.com.alkaff.myfirstapp2019.R;
import sdkjordan.com.alkaff.myfirstapp2019.activities.MediaActivity;
import sdkjordan.com.alkaff.myfirstapp2019.models.Song;


public class AnotherMediaPlayerService extends Service implements
        MediaPlayer.OnPreparedListener,
        MediaPlayer.OnCompletionListener,
        MediaPlayer.OnErrorListener ,
        AudioManager.OnAudioFocusChangeListener{

    public static final String TAG = "MediaPlayerService";
    public static final String ACTION_PLAY = "action_play";
    public static final String ACTION_PAUSE = "action_pause";
    public static final String ACTION_REWIND = "action_rewind";
    public static final String ACTION_FAST_FORWARD = "action_fast_foward";
    public static final String ACTION_NEXT = "action_next";
    public static final String ACTION_PREVIOUS = "action_previous";
    public static final String ACTION_STOP = "action_stop";

    private static final float JUMP_RATIO = 0.05F;
    private static final int NOTIFY_ID = 52;


    //media player
    private MediaPlayer player;

    private MediaSessionManager mManager;
    private MediaSession mSession;
    private MediaController mController;
    private int mLastPosition;
    private int mDuration;
    private int mJumpTime;
    //current position
    private int songPosn;

    private boolean shuffle = false;
    private Random rand;

    AudioManager audioManager ;

    //song list
    private ArrayList<Song> songs;
    private Song playSong;

    private final IBinder musicBind = new MusicBinder();


    private CharSequence songTitle;


    @Override
    public void onCreate() {
        //create the service
        super.onCreate();
        //initialize position
        songPosn = 0;
        //create player
        player = new MediaPlayer();

        rand = new Random();

        audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
//        AudioFocusRequest audioFocusRequest = new AudioFocusRequest();
//        audioManager.requestAudioFocus(new Audi)
    }

    public void initMusicPlayer() {
        //set player properties
        player.setWakeMode(getApplicationContext(),
                PowerManager.PARTIAL_WAKE_LOCK);
        player.setAudioStreamType(AudioManager.STREAM_MUSIC);

        player.setOnPreparedListener(this);
        player.setOnCompletionListener(this);
        player.setOnErrorListener(this);
    }

    private void initMediaSessions() {
        player = MediaPlayer.create(getApplicationContext(), R.raw.badnews);
        player.setLooping(false);

        mDuration = player.getDuration();
        mJumpTime = (int) (mDuration * JUMP_RATIO);

        mSession = new MediaSession(getApplicationContext(), "simple player session");
        mController = new MediaController(getApplicationContext(), mSession.getSessionToken());

        mSession.setCallback(new MediaSession.Callback() {

                                 @Override
                                 public void onPlay() {
//                                     super.onPlay();

                                     if (player.isPlaying()) {
                                         // Rewind to beginning of song
                                         player.seekTo(mLastPosition);

                                     } else {
                                         // Start playing song
                                         player.seekTo(0);
                                         player.start();
                                     }


                                     Log.e("MediaPlayerService", "onPlay");
                                     buildNotification(generateAction(android.R.drawable.ic_media_pause, "Pause", ACTION_PAUSE));
                                 }

                                 @Override
                                 public void onPause() {
                                     mLastPosition = player.getCurrentPosition();

//                                     super.onPause();
                                     player.pause();
                                     Log.e("MediaPlayerService", "onPause");
                                     buildNotification(generateAction(android.R.drawable.ic_media_play, "Play", ACTION_PLAY));
                                 }

                                 @Override
                                 public void onSkipToNext() {
//                                     super.onSkipToNext();
                                     Log.e("MediaPlayerService", "onSkipToNext");
                                     //Change media here
                                     buildNotification(generateAction(android.R.drawable.ic_media_pause, "Pause", ACTION_PAUSE));
                                 }

                                 @Override
                                 public void onSkipToPrevious() {
//                                     super.onSkipToPrevious();
                                     Log.e("MediaPlayerService", "onSkipToPrevious");
                                     //Change media here
                                     buildNotification(generateAction(android.R.drawable.ic_media_pause, "Pause", ACTION_PAUSE));
                                 }

                                 @Override
                                 public void onFastForward() {
                                     super.onFastForward();
                                     Log.e("MediaPlayerService", "onFastForward");
                                     //Manipulate current media here
                                     if (player.isPlaying()) {
                                         if (mLastPosition + mJumpTime > mDuration)
                                             player.seekTo(mDuration);
                                         else
                                             player.seekTo(mLastPosition + mJumpTime);
                                     }

                                 }

                                 @Override
                                 public void onRewind() {
                                     super.onRewind();
                                     Log.e("MediaPlayerService", "onRewind");

                                     //Manipulate current media here

                                     if (player.isPlaying()) {
                                         if (mLastPosition - mJumpTime < 0)
                                             player.seekTo(0);
                                         else
                                             player.seekTo(mLastPosition - mJumpTime);
                                     }
                                 }

                                 @Override
                                 public void onStop() {
                                     super.onStop();
                                     Log.e("MediaPlayerService", "onStop");
                                     //Stop media player here
                                     NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                                     notificationManager.cancel(1);
                                     Intent intent = new Intent(getApplicationContext(), AnotherMediaPlayerService.class);
                                     stopService(intent);
                                 }

                                 @Override
                                 public void onSeekTo(long pos) {
                                     // player.seekTo((int) pos);
                                 }

                                 @Override
                                 public void onSetRating(Rating rating) {
                                     super.onSetRating(rating);
                                 }
                             }
        );
    }

    public void setList(ArrayList<Song> theSongs) {
        songs = theSongs;
    }

    public void setShuffle() {
        if (shuffle) shuffle = false;
        else shuffle = true;
    }

    @Override
    public void onAudioFocusChange(int i) {

    }

    public class MusicBinder extends Binder {
        public AnotherMediaPlayerService getService() {
            return AnotherMediaPlayerService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return musicBind;
    }


    private void handleIntent(Intent intent) {
        if (intent == null || intent.getAction() == null)
            return;

        String action = intent.getAction();

        if (action.equalsIgnoreCase(ACTION_PLAY)) {
            mController.getTransportControls().play();
        } else if (action.equalsIgnoreCase(ACTION_PAUSE)) {
            mController.getTransportControls().pause();
        } else if (action.equalsIgnoreCase(ACTION_FAST_FORWARD)) {
            mController.getTransportControls().fastForward();
        } else if (action.equalsIgnoreCase(ACTION_REWIND)) {
            mController.getTransportControls().rewind();
        } else if (action.equalsIgnoreCase(ACTION_PREVIOUS)) {
            mController.getTransportControls().skipToPrevious();
        } else if (action.equalsIgnoreCase(ACTION_NEXT)) {
            mController.getTransportControls().skipToNext();
        } else if (action.equalsIgnoreCase(ACTION_STOP)) {
            mController.getTransportControls().stop();
        }

    }

    private Notification.Action generateAction(int icon, String title, String intentAction) {
        Intent intent = new Intent(getApplicationContext(), AnotherMediaPlayerService.class);
        intent.setAction(intentAction);
        PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(), 1, intent, 0);
        return new Notification.Action.Builder(icon, title, pendingIntent).build();
    }

    private void buildNotification(Notification.Action action) {
        Notification.MediaStyle style = new Notification.MediaStyle();
        Intent intent = new Intent(getApplicationContext(), AnotherMediaPlayerService.class);
        intent.setAction(ACTION_STOP);
        PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(), 1, intent, 0);
        Notification.Builder builder = new Notification.Builder(this)
                .setSmallIcon(android.R.drawable.ic_media_play)
                .setContentTitle("Media Title")
                .setContentText("Media Artist")
                .setDeleteIntent(pendingIntent)
                .setStyle(style);

        builder.addAction(generateAction(android.R.drawable.ic_media_previous, "Previous", ACTION_PREVIOUS));
        builder.addAction(generateAction(android.R.drawable.ic_media_rew, "Rewind", ACTION_REWIND));
        builder.addAction(action);
        builder.addAction(generateAction(android.R.drawable.ic_media_ff, "Fast Foward", ACTION_FAST_FORWARD));
        builder.addAction(generateAction(android.R.drawable.ic_media_next, "Next", ACTION_NEXT));
        style.setShowActionsInCompactView(0, 1, 2, 3, 4);

        // NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        startForeground(1, builder.build());
        //notificationManager.notify(1, builder.build());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (mManager == null) {
            initMediaSessions();
        }

        handleIntent(intent);
        return super.onStartCommand(intent, flags, startId);
    }


    @Override
    public boolean onUnbind(Intent intent) {
        mSession.release();
        player.stop();
        player.release();
        return false;
    }


    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        //start playback
        if (mediaPlayer != null) {
            mediaPlayer.start();
            Intent notIntent = new Intent(this, MediaActivity.class);
            notIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendInt = PendingIntent.getActivity(this, 0,
                    notIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            Notification.Builder builder = new Notification.Builder(this);

            builder.setContentIntent(pendInt)
                    .setSmallIcon(android.R.drawable.ic_media_play)
                    .setTicker(songTitle)
                    .setOngoing(true)
                    .setContentTitle(getResources().getString(R.string.txt_playing))
                    .setContentText(songTitle);
            Notification not = builder.build();

            startForeground(NOTIFY_ID, not);
        }

    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        if (player.getCurrentPosition() > 0) {
            mediaPlayer.reset();
            playNext();
        }


    }

    @Override
    public void onDestroy() {
        stopForeground(true);
    }

    @Override
    public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
        mediaPlayer.reset();

        return false;
    }

    public int getPosn() {
        return player.getCurrentPosition();
    }

    public int getDur() {
        return player.getDuration();
    }

    public boolean isPng() {
        return player.isPlaying();
    }

    public void pausePlayer() {
        player.pause();
    }

    public void seek(int posn) {
        player.seekTo(posn);
    }

    public void go() {
        player.start();
    }

    public void setSong(int songIndex) {
        songPosn = songIndex;
    }

    public void playPrev() {
        songPosn--;
        if (songPosn < 0) songPosn = songs.size() - 1;
        playSong(songPosn);
        songTitle = playSong.getTitle();
    }

    public void playSong(int pos) {
        //get song
        Song playSong = songs.get(songPosn);
        //get id
        int currSong = playSong.getID();

        player = MediaPlayer.create(this, R.raw.badnews);
        Log.w(TAG, String.format("Track %s is prepared ",playSong.getTitle()));
        if (player == null)
            Log.e(TAG, "Error setting data source");

    }

    //skip to next
    public void playNext() {
        if (shuffle) {
            int newSong = songPosn;
            while (newSong == songPosn) {
                newSong = rand.nextInt(songs.size());
            }
            songPosn = newSong;
        } else {
            songPosn++;
            if (songPosn >= songs.size()) songPosn = 0;
        }
        playSong(songPosn);
    }


}
