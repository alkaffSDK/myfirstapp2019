package sdkjordan.com.alkaff.myfirstapp2019.services;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.Nullable;

import java.net.URL;

import sdkjordan.com.alkaff.myfirstapp2019.Constants;

public class MyIntentService extends IntentService {

    public static final String TAG = "MyIntentService";
    public MyIntentService() {
        super("MyIntentService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        // TODO: Do you work here

        String data = "";
        if(null != intent)
            data = intent.getStringExtra(Constants.Extras.EXTRA_DATA);

        try {
            Thread.sleep(1000);
            Log.i(TAG,"IntentService with Data:"+data);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        MyIntentService2.startActionStart(this,"","");
        MyIntentService2.startActionPause(getApplicationContext());
        MyIntentService2.startActionPause(this);
    }
}
