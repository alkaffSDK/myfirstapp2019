package sdkjordan.com.alkaff.myfirstapp2019.services;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class MyIntentService2 extends IntentService {
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_START = "sdkjordan.com.alkaff.myfirstapp2019.services.action.START";
    private static final String ACTION_STOP = "sdkjordan.com.alkaff.myfirstapp2019.services.action.STOP";
    private static final String ACTION_PAUSE = "sdkjordan.com.alkaff.myfirstapp2019.services.action.PAUSE";


    // TODO: Rename parameters
    private static final String EXTRA_FILE_URL = "sdkjordan.com.alkaff.myfirstapp2019.services.extra.FILE_URL";
    private static final String EXTRA_FILE_PATH = "sdkjordan.com.alkaff.myfirstapp2019.services.extra.FILE_PATH";

    public MyIntentService2() {
        super("MyIntentService2");
    }

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionStart(Context context, String url, String path) {
        Intent intent = new Intent(context, MyIntentService2.class);
        intent.setAction(ACTION_START);
        intent.putExtra(EXTRA_FILE_URL, url);
        intent.putExtra(EXTRA_FILE_PATH, path);
        context.startService(intent);
    }

    /**
     * Starts this service to perform action Baz with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionStop(Context context) {
        Intent intent = new Intent(context, MyIntentService2.class);
        intent.setAction(ACTION_STOP);
        context.startService(intent);
    }


    public static void startActionPause(Context context) {
        Intent intent = new Intent(context, MyIntentService2.class);
        intent.setAction(ACTION_PAUSE);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_START.equals(action)) {
                final String url = intent.getStringExtra(EXTRA_FILE_URL);
                final String path = intent.getStringExtra(EXTRA_FILE_PATH);
                handleActionStart(url, path);
            } else if (ACTION_STOP.equals(action)) {
                handleActionStop();
            } else if (ACTION_PAUSE.equals(action)) {
                handleActionPause();
            }
        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionStart(String url, String path) {
        // TODO: Handle action Foo
        throw new UnsupportedOperationException("Not yet implemented");
    }

    /**
     * Handle action Baz in the provided background thread with the provided
     * parameters.
     */
    private void handleActionStop() {
        // TODO: Handle action Baz
        throw new UnsupportedOperationException("Not yet implemented");
    }


    private void handleActionPause() {
        // TODO: Handle action Baz
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
