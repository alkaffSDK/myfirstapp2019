package com.sdkjordan.alkaff.contentprovider;

class DataRecord {

	private static int id;

	// Unique ID
	private final int _id;

	// Display Name
	private  String _data;

	DataRecord(String _data) {
		this._data = _data;
		this._id = id++;
	}


	public void setData(String data) {
		_data = data ;
	}

	String getData() {
		return _data;
	}

	int getID() {
		return _id;
	}

}
