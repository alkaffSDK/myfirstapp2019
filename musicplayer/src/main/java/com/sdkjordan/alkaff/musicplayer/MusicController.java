package com.sdkjordan.alkaff.musicplayer;

import android.content.Context;
import android.widget.MediaController;


/**
 * The MediaController class presents a standard widget with start/pause, rewind, fast-forward, and skip (previous/next) buttons in it
 */
public class MusicController extends MediaController {


    public MusicController(Context c){
        super(c);
    }

    public void hide(){}
}
